#简介
在了解jopencart之前首先大概介绍下OpenCart,OpenCart 是世界著名的开源电子商务系统，系统开发语言为 PHP,OpenCart 已成为世界主流的电子商务建站系统。该项目采用 MVCL 架构。

jopencart是一个OpenCart的JAVA部分移植版本，未来将支持OpenCart的全部功能，项目也采用MVCL架构，其中的VL将使用OpenCart原版部分，MC换成JAVA实现。通俗点说就是将OpenCart换上JAVA引擎。主要目的是充分发挥Java在缓存、多线程、数据库连接池等方面的优势提高OpenCart性能。

#演示地址
在线演示：[http://www.javacart.cn/](http://www.javacart.cn/)
演示用户账号：admin@javacart.cn / 123456

#为什么做这项目
1. 最近两年接了不少OpenCart二次开发单子，深刻体会到OpenCart优秀的MVCL架构。
2. OpenCart在数据量较大情况是性能较低，缓存设计的不是很理想、Opencart部分插件SQL注入风险较大。
3. OpenCart在搜索方面使用LIKE实现，如果能直接用上Lucene岂不更好。
4. JAVA竟然没个完美的开源开源商城？

#技术方面
1. 项目使用JFINAL作为MVC、ORM基础框架。
2. 使用PHP作为模板引擎。
3. 多数据库支持，读写分离支持。
4. 将页面模块化、碎片化，直接做页面级别缓存大大提高性能。
5. 支持分布式部署。

#功能方面
1. 陆续将移植opencart基本全部功能。
2. 加入特色功能微信商城。
3. 加入支付宝支付、微信支付。
4. 第三方登录，支持微信、QQ、微博等。
5. 多语言、多货币；不限分类\商品；丰富支付方式、模板、模块；代码开源、MVC架构，易学易用！

#性能方面
1. 优化Opencart的SQL。
2. 使用Druid进行SQL监控。
3. 搜索使用Lucene实现，提高搜索性能。
4. 大量的缓存设计。
5. 分布式设计，可以通过简单的添加机器来支撑更大的并发。

#最最重要
数据设计完全复用OpenCart-2.2.0.0，原版OpenCart网站迁移几乎0成本。

#编译说明
JPHP需要在github自己下载项目编译并发布到本地MAVNE库。

```
<dependency>
	<groupId>cn.javacart</groupId>
	<artifactId>jfinal-php-render</artifactId>
	<version>0.0.1-SNAPSHOT</version>
</dependency>
<dependency>
	<groupId>com.jfplugin</groupId>
	<artifactId>jfinal-mockrender-kit</artifactId>
	<version>2.2</version>
</dependency>
```
这两个也在我的osc项目里开源，自行下载编译。

#来几张图
JAVA版本Jopencart截图，和OPENCART的一致。

![JAVA版本](http://git.oschina.net/uploads/images/2017/0114/000145_bea8e6bb_369917.png "首页")
商品详情页面
![详情页面](http://git.oschina.net/uploads/images/2017/0113/235859_edbfae7b_369917.png "商品详情页面")
用户登录界面
![登录界面](http://git.oschina.net/uploads/images/2017/0111/225129_13256358_369917.png "登录界面")
用户中心
![用户中心](http://git.oschina.net/uploads/images/2017/0114/000318_0bb91b9a_369917.png "用户中心")

#功能完成清单

| 功能模块      | 完成情况 |
| :-------: |:----: |
| 商城首页  | 完成  |
| 用户登录  | 完成  |
| 注销登录  | 完成  |
| 用户首页  | 完成  |
| 商品详情页面  | 完成  |
| 商品评价  | 未完成  |
| 商品分类页面  | 进行中  |