package cn.javacart.jopencart.kit;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

import org.jsoup.Jsoup;

/**
 * 字符串工具
 * @author farmer
 *
 */
public class StrTool {

	/**
	 * 解码
	 * 
	 * @param src
	 * @return
	 */
	public static String decode(String src) {
		try {
			return URLDecoder.decode(src, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException("解码错误");
		}
	}

	/**
	 * 编码
	 * 
	 * @param src
	 * @return
	 */
	public static String encode(String src) {
		try {
			return URLEncoder.encode(src, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException("编码错误");
		}
	}
	
	/**
	 * 清除标签后截取
	 * @param src
	 * @param start
	 * @param length
	 * @return
	 */
	public static String subStrClearTag(String src,int start ,int length){
		String parse = Jsoup.parse(src).text().substring(start);
		return (parse.length() > length ? parse.substring(start, length) : parse) + "...";
	}
	
}
