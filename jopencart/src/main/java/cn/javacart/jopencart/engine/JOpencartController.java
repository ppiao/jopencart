package cn.javacart.jopencart.engine;

import java.util.HashMap;
import java.util.Map;

import php.runtime.Memory;
import cn.javacart.jopencart.library.ConfigService;
import cn.javacart.jopencart.library.DocumentService;
import cn.javacart.jopencart.library.LanguageService;
import cn.javacart.jopencart.library.LoadService;
import cn.javacart.jopencart.library.UrlService;
import cn.javacart.jopencart.library.cart.CurrencyService;
import cn.javacart.jopencart.library.cart.CustomerService;
import cn.javacart.jopencart.library.cart.TaxService;

import com.jfinal.core.Controller;

/**
 * 基类
 * @author farmer
 *
 */
public class JOpencartController extends Controller{
	
	/**
	 * 语言
	 */
	protected LanguageService language = LanguageService.getInstance();
	
	/**
	 * 配置
	 */
	protected ConfigService config = ConfigService.getInstance();
	
	/**
	 * 线程下的单例
	 */
	protected DocumentService document = DocumentService.getThreadInstance();
	
	/**
	 * 组件加载服务
	 */
	protected LoadService load = LoadService.getInstance();
	
	/**
	 * URL服务
	 */
	protected UrlService url = UrlService.getInstance();
	
	/**
	 * 货币服务
	 */
	protected CurrencyService currency = CurrencyService.getInstance();
	
	/**
	 * 税服务
	 */
	protected TaxService tax = TaxService.ME;
	
	/**
	 * 客户当前客户对象
	 */
	protected CustomerService customer = null;
	
	/**
	 * 数据
	 */
	protected Map<Object, Memory> dataMap = new HashMap<Object, Memory>();
	
	/**
	 * 路由
	 */
	protected String route = null;
	
	/**
	 * Token
	 */
	protected String token = null;
	
	/**
	 * 是否为post
	 */
	protected boolean post = false;
	
	/**
	 * 错误Map
	 */
	protected Map<String, String> errorMap = new HashMap<String, String>();
	
	/**
	 * 
	 */
	protected String configTheme = ConfigService.getInstance().get("config_theme");
	
	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	public CustomerService getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerService customer) {
		this.customer = customer;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public boolean isPost() {
		return post;
	}

	public void setPost(boolean post) {
		this.post = post;
	}

	public Map<String, String> getErrorMap() {
		return errorMap;
	}

	public void setErrorMap(Map<String, String> errorMap) {
		this.errorMap = errorMap;
	}
	
}
