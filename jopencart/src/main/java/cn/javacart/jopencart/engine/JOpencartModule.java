package cn.javacart.jopencart.engine;


/**
 * 组件基类
 * @author farmer
 *
 */
public abstract class JOpencartModule extends JOpencartController{

	/**
	 * 
	 */
	protected JOpencartController controller = null;
	
	protected static ThreadLocal<Integer> module = new ThreadLocal<Integer>(){
		@Override
		public Integer get() {
			Integer i = super.get();
			set(i+1);
			return i;
		}
	};
	{
		module.set(1);
	}
	public JOpencartModule() { }
	
	/**
	 * 构造
	 * @param controller
	 */
	public JOpencartModule(JOpencartController controller) {
		this.controller = controller;
		this.config = controller.config;
		this.document = controller.document;
		this.language = controller.language;
		this.load = controller.load;
		this.route = controller.route;
		this.url = controller.url;
		this.customer = controller.customer;
		this.currency = controller.currency;
		this.tax = controller.tax;
	}
	
	public <T> T getSessionAttr(String key){
		if(controller != null){
			return controller.getSessionAttr(key);
		}
		return super.getSessionAttr(key);
	}
	
	
	
	@Override
	public String getPara(String name) {
		if(controller != null){
			return controller.getPara(name);
		}
		return super.getPara(name);
	}

	@Override
	public String getPara(String name, String defaultValue) {
		if(controller != null){
			return controller.getPara(name,defaultValue);
		}
		return super.getPara(name, defaultValue);
	}

	/**
	 * 渲染
	 * @return
	 */
	public abstract String render();
	
}
