package cn.javacart.jopencart.controller.catalog.module;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringEscapeUtils;

import php.runtime.memory.support.MemoryUtils;
import cn.javacart.jfinal.php.render.PhpRender;
import cn.javacart.jopencart.engine.JOpencartController;
import cn.javacart.jopencart.engine.JOpencartModule;
import cn.javacart.jopencart.kit.StrTool;
import cn.javacart.jopencart.model.Product;
import cn.javacart.jopencart.service.catalog.catalog.CatalogProductService;

import com.alibaba.fastjson.JSON;
import com.jfinal.kit.StrKit;
import com.jfplugin.kit.mockrender.MockRenderKit;

/**
 * 推荐商品
 * @author farmer
 *
 */
public class ModuleFeaturedModule extends JOpencartModule{

	private String extra = null;
	
	public ModuleFeaturedModule(JOpencartController controller) {
		super(controller);
	}
	
	public ModuleFeaturedModule(JOpencartController controller,Object extra) {
		super(controller);
		this.extra = (String) extra;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String render() {
		language.load("/catalog/language/{0}/module/featured.php");
		dataMap.put("heading_title", language.get("heading_title"));
		dataMap.put("text_tax", language.get("text_tax"));
		dataMap.put("button_cart", language.get("button_cart"));
		dataMap.put("button_wishlist", language.get("button_wishlist"));
		dataMap.put("button_compare", language.get("button_compare"));
		HashMap<String,Object> settingMap = JSON.parseObject(extra, HashMap.class);
		int limit = 4;
		if(StrKit.notBlank((String)settingMap.get("limit"))){
			limit = Integer.parseInt((String)settingMap.get("limit"));
		}
		List<Map<String, Object>> productList = new ArrayList<Map<String,Object>>();
		List<Object> products = (List<Object>) settingMap.get("product");
		int size = products.size();
		for (int i = 0; i < (limit>size?size:limit); i++) {
			Product p = CatalogProductService.ME.getProduct(Integer.parseInt(""+products.get(i)));
			Map<String, Object> productMap = new HashMap<String, Object>();
			String priceValue = null;
			BigDecimal price = p.getBigDecimal("price");
			if(customer.isLogged() || config.isset("config_customer_price")){
				priceValue = currency.format(tax.calculate(price, p.getInt("tax_class_id"), (String)config.get("config_tax")),
						(String)getSessionAttr("currency"), null);
			}
			String specialValue = null;
			BigDecimal special = p.getBigDecimal("special");
			if(special != null){
				specialValue = currency.format(tax.calculate(special, p.getInt("tax_class_id"), (String)config.get("config_tax")),
						(String)getSessionAttr("currency"), null);
			}
			String taxValue = null;
			if(config.isset("config_tax")){
				taxValue = currency.format(special == null ?price : special,
						(String)getSessionAttr("currency"), null);
			}
			productMap.put("price", priceValue);
			productMap.put("special", specialValue);
			productMap.put("tax", taxValue);
			productMap.put("description",   StrTool.subStrClearTag(StringEscapeUtils.unescapeHtml4(p.getStr("description")), 0, 100));
			String uri = p.getStr("image");
			productMap.put("thumb", uri.substring(0, uri.lastIndexOf("."))+"_"+settingMap.get("width")+"x"+settingMap.get("height")+uri.substring(uri.lastIndexOf("."),uri.length()));
			productMap.put("name", p.getStr("name"));
			productMap.put("href", url.linkStr("product/product", "product_id="+p.get("product_id")));
			productList.add(productMap);
		}
		if(productList.size() > 0){
			dataMap.put("products", MemoryUtils.valueOf(productList));			
			return MockRenderKit.render(new PhpRender("/catalog/view/theme/default/template/module/featured.tpl", dataMap));
		}else{
			return "";
		}
	}

}
