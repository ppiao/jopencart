package cn.javacart.jopencart.controller.catalog.module;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import php.runtime.memory.support.MemoryUtils;
import cn.javacart.jfinal.php.render.PhpRender;
import cn.javacart.jopencart.engine.JOpencartController;
import cn.javacart.jopencart.engine.JOpencartModule;
import cn.javacart.jopencart.kit.ImageKit;
import cn.javacart.jopencart.model.BannerImage;
import cn.javacart.jopencart.service.catalog.design.DesignBannerService;

import com.alibaba.fastjson.JSON;
import com.jfplugin.kit.mockrender.MockRenderKit;

/**
 * 幻灯片
 * @author farmer
 *
 */
public class ModuleCarouselModule extends JOpencartModule{

	private String extra = null;
	
	public ModuleCarouselModule(JOpencartController controller) {
		super(controller);
	}
	
	public ModuleCarouselModule(JOpencartController controller,Object extra) {
		super(controller);
		this.extra = (String) extra;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String render() {
		document.addStyle("catalog/view/javascript/jquery/owl-carousel/owl.carousel.css", null,null);
		document.addScript("catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js", null);
		HashMap<String,String> settingMap = JSON.parseObject(extra, HashMap.class);
		String bannerId = settingMap.get("banner_id");
		List<Map<String, Object>> banners = new ArrayList<Map<String,Object>>();
		for (BannerImage bannerImage : DesignBannerService.ME.getBanner(Integer.parseInt(bannerId))) {
			Map<String, Object> bannerItem = new HashMap<String, Object>();
			bannerItem.put("title", bannerImage.get("title"));
			bannerItem.put("link", bannerImage.get("link"));
			String uri = bannerImage.getStr("image");
			bannerItem.put("image", ImageKit.resize(uri, settingMap.get("width"), settingMap.get("height")));
			banners.add(bannerItem);
		}
		dataMap.put("banners", MemoryUtils.valueOf(banners));
		dataMap.put("module", MemoryUtils.valueOf(module.get()+1));
		return MockRenderKit.render(new PhpRender("/catalog/view/theme/default/template/module/carousel.tpl", dataMap));
	}

}
