package cn.javacart.jopencart.controller.catalog.common;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import php.runtime.memory.support.MemoryUtils;
import cn.javacart.jfinal.php.render.PhpRender;
import cn.javacart.jopencart.engine.JOpencartController;
import cn.javacart.jopencart.engine.JOpencartModule;
import cn.javacart.jopencart.model.Currency;

import com.jfplugin.kit.mockrender.MockRenderKit;

/**
 * 语言组件
 * @author farmer
 *
 */
public class CommonCurrencyModule extends JOpencartModule{

	public CommonCurrencyModule() {}
	
	public CommonCurrencyModule(JOpencartController controller) {
		super(controller);
	}

	@Override
	public String render() {
		language.load("/catalog/language/{0}/common/currency.php");	//{1}表示语言CODE
		//$data['code'] = $this->session->data['currency'];
		dataMap.put("code", MemoryUtils.valueOf("USD"));
		dataMap.put("text_currency", language.get("text_currency"));
		dataMap.put("action", url.link("common/currency/currency", ""));
		List<Map<String, Object>> currencieList = new ArrayList<Map<String,Object>>();
		List<Currency> currencies = Currency.ME.find("select t.title,t.code,t.symbol_left,t.symbol_right from joc_currency t");
		for (Currency currency : currencies) {
			currencieList.add(currency.toRecord().getColumns());
		}
		dataMap.put("currencies",  MemoryUtils.valueOf(currencieList));
		dataMap.put("redirect",  url.link("common/home", ""));
		return MockRenderKit.render(new PhpRender("/catalog/view/theme/default/template/common/currency.tpl", dataMap));
	}
	
	/**
	 * 跳转
	 */
	public void currency(){
		redirect(getPara("redirect",url.linkStr("common/home", "")));
	}
	
}