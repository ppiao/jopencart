package cn.javacart.jopencart.controller.catalog.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import php.runtime.memory.support.MemoryUtils;
import cn.javacart.jfinal.php.render.PhpRender;
import cn.javacart.jopencart.engine.JOpencartController;
import cn.javacart.jopencart.engine.JOpencartModule;
import cn.javacart.jopencart.kit.SetKit;
import cn.javacart.jopencart.model.Category;
import cn.javacart.jopencart.service.catalog.catalog.CatalogProductService;
import cn.javacart.jopencart.util.ChainMap;

import com.jfplugin.kit.mockrender.MockRenderKit;

/**
 * 页头
 * @author farmer
 *
 */
public class CommonHeaderModule extends JOpencartModule{

	public CommonHeaderModule(JOpencartController controller) {
		super(controller);
	}

	@Override
	public String render() {
		/*文档相关参数*/
		dataMap.put("title", MemoryUtils.valueOf(document.getTitle()));
		dataMap.put("description", MemoryUtils.valueOf(document.getDescription()));
		dataMap.put("keywords", MemoryUtils.valueOf(document.getKeywords()));
		dataMap.put("links", MemoryUtils.valueOf(document.getLinks()));
		dataMap.put("styles", MemoryUtils.valueOf(document.getStyles()));
		dataMap.put("scripts", MemoryUtils.valueOf(document.getScripts(null)));
		dataMap.put("analytics", MemoryUtils.valueOf( new HashMap<String, String>()));
		dataMap.put("lang", language.get("code"));
		dataMap.put("direction", language.get("direction"));
		dataMap.put("logo", MemoryUtils.valueOf(((String)config.get("context_path")).concat("image/").concat((String)config.get("config_logo"))));
		language.load("/catalog/language/{0}/common/header.php");
		dataMap.put("text_shopping_cart", language.get("text_shopping_cart"));
		dataMap.put("text_account", language.get("text_account"));
		dataMap.put("text_register", language.get("text_register"));
		dataMap.put("text_login", language.get("text_login"));
		dataMap.put("text_order", language.get("text_order"));
		dataMap.put("text_transaction", language.get("text_transaction"));
		dataMap.put("text_download", language.get("text_download"));
		dataMap.put("text_logout", language.get("text_logout"));
		dataMap.put("text_checkout", language.get("text_checkout"));
		dataMap.put("text_category", language.get("text_category"));
		dataMap.put("text_all", language.get("text_all"));
		//愿望清单
		Map<Integer, Integer> wishlist = getSessionAttr("wishlist");
		if(getSessionAttr("wishlist") != null){
			dataMap.put("text_wishlist",MemoryUtils.valueOf(String.format(language.getStr("text_wishlist"), wishlist.size())));
		}
		else{			
			dataMap.put("text_wishlist",MemoryUtils.valueOf(String.format(language.getStr("text_wishlist"), 0)));
		}
		dataMap.put("home",url.link("common/home", ""));
		dataMap.put("wishlist", url.link("account/wishlist", ""));
		dataMap.put("logged", MemoryUtils.valueOf(customer.isLogged()));	//登录状态
		dataMap.put("account", url.link("account/account", ""));
		dataMap.put("register", url.link("account/register", ""));
		dataMap.put("login", url.link("account/login", ""));
		dataMap.put("order", url.link("account/order", ""));
		dataMap.put("transaction", url.link("account/transaction", ""));
		dataMap.put("download", url.link("account/download", ""));
		dataMap.put("logout", url.link("account/logout", ""));
		dataMap.put("shopping_cart", url.link("checkout/cart", ""));
		dataMap.put("checkout", url.link("checkout/checkout", ""));
		dataMap.put("contact", url.link("information/contact", ""));
		dataMap.put("telephone", MemoryUtils.valueOf(config.get("config_telephone")));
		
		List<Map<String, Object>> categorieList = new ArrayList<Map<String,Object>>();
		List<Category> categories = Category.ME.getCategories(0);
		for (Category category : categories) {
			if(category.get("top") != null){
				List<Map<String, Object>> childrenData = new ArrayList<Map<String,Object>>();
				List<Category> children = Category.ME.getCategories(category.getInt("category_id"));
				for (Category child : children) {
					Map<String, Object> childrenDataItem = new HashMap<String, Object>();
					childrenDataItem.put("name", category.getStr("name")  + (SetKit.isset(config.getInt("config_product_count")) ?  "(" + 
							CatalogProductService.ME.getTotalProducts(
									ChainMap.createMap().put("filter_category_id", 
											child.getInt("category_id")).put("filter_sub_category", true).toMap())
							+")" : ""));
					childrenDataItem.put("href", url.link("product/category", "path="+category.getInt("category_id")+"_"+child.getInt("category_id")));					
					childrenData.add(childrenDataItem);
				}
				Map<String,Object> categoryMap = new HashMap<String, Object>();
				categoryMap.put("name", category.get("name"));
				categoryMap.put("children", childrenData);
				categoryMap.put("column", (category.get("column") != null && category.getInt("column") != 0 )? category.get("column") : 1);
				categoryMap.put("href", url.link("product/category", "path="+category.getInt("category_id")));
				categorieList.add(categoryMap);
			}
		}
		dataMap.put("categories", MemoryUtils.valueOf(categorieList));
		dataMap.put("class",MemoryUtils.valueOf("common-home"));
		/*页头组件*/
		dataMap.put("language", MemoryUtils.valueOf(load.module(new CommonLanguageModule(controller))));	//加载语言
		dataMap.put("currency", MemoryUtils.valueOf(load.module(new CommonCurrencyModule(controller))));	//加载币种
		dataMap.put("search", MemoryUtils.valueOf(load.module(new CommonSearchModule(controller))));	//加载搜索框
		dataMap.put("cart", MemoryUtils.valueOf(load.module(new CommonCartModule(controller))));	//加载购物车		
		return MockRenderKit.render(new PhpRender("/catalog/view/theme/default/template/common/header.tpl", dataMap));
	}

}
