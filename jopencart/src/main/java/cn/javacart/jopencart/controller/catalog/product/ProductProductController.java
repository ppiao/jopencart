package cn.javacart.jopencart.controller.catalog.product;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringEscapeUtils;

import php.runtime.memory.StringMemory;
import php.runtime.memory.support.MemoryUtils;
import cn.javacart.jfinal.php.render.PhpRender;
import cn.javacart.jopencart.controller.catalog.common.CommonColumnLeftModule;
import cn.javacart.jopencart.controller.catalog.common.CommonColumnRightModule;
import cn.javacart.jopencart.controller.catalog.common.CommonContentBottomModule;
import cn.javacart.jopencart.controller.catalog.common.CommonContentTopModule;
import cn.javacart.jopencart.controller.catalog.common.CommonFooterModule;
import cn.javacart.jopencart.controller.catalog.common.CommonHeaderModule;
import cn.javacart.jopencart.engine.JOpencartController;
import cn.javacart.jopencart.kit.ImageKit;
import cn.javacart.jopencart.kit.SetKit;
import cn.javacart.jopencart.kit.StrTool;
import cn.javacart.jopencart.model.Category;
import cn.javacart.jopencart.model.Manufacturer;
import cn.javacart.jopencart.model.Product;
import cn.javacart.jopencart.model.ProductDiscount;
import cn.javacart.jopencart.model.ProductImage;
import cn.javacart.jopencart.model.ProductOption;
import cn.javacart.jopencart.model.ProductOptionValue;
import cn.javacart.jopencart.service.catalog.catalog.CatalogCategoryService;
import cn.javacart.jopencart.service.catalog.catalog.CatalogManufacturerService;
import cn.javacart.jopencart.service.catalog.catalog.CatalogProductService;
import cn.javacart.jopencart.util.ChainMap;

import com.jfinal.kit.StrKit;
import com.jfinal.render.HtmlRender;

/**
 * 商品界面
 * @author farmer
 * 
 */
public class ProductProductController extends JOpencartController{
	
	/**
	 * 商品详细页面
	 */
	public void index(){
		language.load("/catalog/language/{0}/product/product.php");
		List<Map<String, Object>> breadcrumbs = new ArrayList<Map<String,Object>>();
		breadcrumbs.add(ChainMap.createMap().put("text", language.getStr("text_home")).put("href", url.linkStr("common/home", null)).toMap());
		if(StrKit.notBlank(getPara("path"))){
			String path = null;
			List<String> parts = new ArrayList<String>(Arrays.asList(getPara("path").split("_")));
			int size = parts.size();
			Integer categoryId = Integer.parseInt(parts.get(size-1));
			parts.remove(size-1);
			for (String pathId : parts) {
				if(path == null){
					path = pathId;
				} else {
					path =path + "_" + pathId;
				}
				Category categoryInfo = CatalogCategoryService.ME.getCategory(Integer.parseInt(pathId));
				if(categoryInfo != null){
					breadcrumbs.add(ChainMap.createMap().put("text", categoryInfo.get("name")).put("href", url.linkStr("product/category", "path="+path)).toMap());					
				}
			}
			Category categoryInfo = CatalogCategoryService.ME.getCategory(categoryId);
			if(categoryInfo != null){
				StringBuilder urlBuilder = new StringBuilder();
				if(StrKit.notBlank(getPara("sort"))){
					urlBuilder.append("&sort=").append(getPara("sort"));
				}
				if(StrKit.notBlank(getPara("order"))){
					urlBuilder.append("&order=").append(getPara("order"));
				}
				if(StrKit.notBlank(getPara("page"))){
					urlBuilder.append("&page=").append(getPara("page"));
				}
				if(StrKit.notBlank(getPara("limit"))){
					urlBuilder.append("&limit=").append(getPara("limit"));
				}
				breadcrumbs.add(ChainMap.createMap().put("text", categoryInfo.get("name")).put("href", url.linkStr("product/category", "path="+getPara("path") + urlBuilder.toString())).toMap());					
			}
		}
		Integer manufacturerId = getParaToInt("manufacturer_id");
		if(manufacturerId != null){
			breadcrumbs.add(ChainMap.createMap().put("text", language.get("text_brand")).put("href", url.linkStr("product/manufacturer",null)).toMap());					
			StringBuilder urlBuilder = new StringBuilder();
			if(StrKit.notBlank(getPara("sort"))){
				urlBuilder.append("&sort=").append(getPara("sort"));
			}
			if(StrKit.notBlank(getPara("order"))){
				urlBuilder.append("&order=").append(getPara("order"));
			}
			if(StrKit.notBlank(getPara("page"))){
				urlBuilder.append("&page=").append(getPara("page"));
			}
			if(StrKit.notBlank(getPara("limit"))){
				urlBuilder.append("&limit=").append(getPara("limit"));
			}
			Manufacturer manufacturerInfo = CatalogManufacturerService.ME.getManufacturer(manufacturerId);
			if(manufacturerInfo != null){
				breadcrumbs.add(ChainMap.createMap().put("text", manufacturerInfo.get("name"))
						.put("href", url.linkStr("product/manufacturer/info", "manufacturer_id="+manufacturerId + urlBuilder.toString())).toMap());					
			}
		}
		if(StrKit.notBlank(getPara("search")) || StrKit.notBlank(getPara("tag"))){
			StringBuilder urlBuilder = new StringBuilder();
			
			if(StrKit.notBlank(getPara("search"))){
				urlBuilder.append("&search=").append(getPara("search"));
			}
			if(StrKit.notBlank(getPara("tag"))){
				urlBuilder.append("&tag=").append(getPara("tag"));
			}
			if(StrKit.notBlank(getPara("description"))){
				urlBuilder.append("&description=").append(getPara("description"));
			}
			if(StrKit.notBlank(getPara("category_id"))){
				urlBuilder.append("&category_id=").append(getPara("category_id"));
			}
			if(StrKit.notBlank(getPara("sub_category"))){
				urlBuilder.append("&sub_category=").append(getPara("sub_category"));
			}
			if(StrKit.notBlank(getPara("sort"))){
				urlBuilder.append("&sort=").append(getPara("sort"));
			}
			if(StrKit.notBlank(getPara("order"))){
				urlBuilder.append("&order=").append(getPara("order"));
			}
			if(StrKit.notBlank(getPara("page"))){
				urlBuilder.append("&page=").append(getPara("page"));
			}
			if(StrKit.notBlank(getPara("limit"))){
				urlBuilder.append("&limit=").append(getPara("limit"));
			}
			breadcrumbs.add(ChainMap.createMap().put("text", language.get("text_search")).put("href", url.linkStr("product/search",urlBuilder.toString())).toMap());
		}
		Integer productId = getParaToInt("product_id", 0);	//商品ID
		Product productInfo = CatalogProductService.ME.getProduct(productId);
		if(productInfo != null){
			StringBuilder urlBuilder = new StringBuilder();
			if(StrKit.notBlank(getPara("path"))){
				urlBuilder.append("&path=").append(getPara("path"));
			}
			if(StrKit.notBlank(getPara("filter"))){
				urlBuilder.append("&filter=").append(getPara("filter"));
			}
			if(StrKit.notBlank(getPara("manufacturer_id"))){
				urlBuilder.append("&manufacturer_id=").append(getPara("manufacturer_id"));
			}
			if(StrKit.notBlank(getPara("search"))){
				urlBuilder.append("&search=").append(getPara("search"));
			}
			if(StrKit.notBlank(getPara("tag"))){
				urlBuilder.append("&tag=").append(getPara("tag"));
			}
			if(StrKit.notBlank(getPara("description"))){
				urlBuilder.append("&description=").append(getPara("description"));
			}
			if(StrKit.notBlank(getPara("category_id"))){
				urlBuilder.append("&category_id=").append(getPara("category_id"));
			}
			if(StrKit.notBlank(getPara("sub_category"))){
				urlBuilder.append("&sub_category=").append(getPara("sub_category"));
			}
			if(StrKit.notBlank(getPara("sort"))){
				urlBuilder.append("&sort=").append(getPara("sort"));
			}
			if(StrKit.notBlank(getPara("order"))){
				urlBuilder.append("&order=").append(getPara("order"));
			}
			if(StrKit.notBlank(getPara("page"))){
				urlBuilder.append("&page=").append(getPara("page"));
			}
			if(StrKit.notBlank(getPara("limit"))){
				urlBuilder.append("&limit=").append(getPara("limit"));
			}
			urlBuilder.append("&product_id=").append(getPara("product_id"));
			breadcrumbs.add(ChainMap.createMap().put("text", productInfo.get("name")).put("href", url.linkStr("product/product",urlBuilder.toString())).toMap());
			document.setTitle(productInfo.getStr("meta_title"));
			document.setDescription(productInfo.getStr("meta_description"));
			document.setKeywords(productInfo.getStr("meta_keyword"));
			document.addLink(url.linkStr("product/product", "product_id="+productId), "canonical");
			document.addScript("catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js", null);
			document.addStyle("catalog/view/javascript/jquery/magnific/magnific-popup.css", null, null);
			document.addScript("catalog/view/javascript/jquery/datetimepicker/moment.js", null);
			document.addScript("catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js", null);
			document.addStyle("catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css", null, null);
			
			dataMap.put("heading_title", MemoryUtils.valueOf(productInfo.getStr("name")));
			dataMap.put("text_select",language.get("text_select"));
			dataMap.put("text_manufacturer",language.get("text_manufacturer"));
			dataMap.put("text_model",language.get("text_model"));
			dataMap.put("text_reward",language.get("text_reward"));
			dataMap.put("text_points",language.get("text_points"));
			dataMap.put("text_stock",language.get("text_stock"));
			dataMap.put("text_discount",language.get("text_discount"));
			dataMap.put("text_tax",language.get("text_tax"));
			dataMap.put("text_option",language.get("text_option"));
			
			dataMap.put("text_minimum",  MemoryUtils.valueOf(String.format(language.getStr("text_minimum"), productInfo.get("minimum"))));
			dataMap.put("text_write",language.get("text_write"));
			dataMap.put("text_login",MemoryUtils.valueOf(String.format(language.getStr("text_login"),  url.linkStr("account/login", null),url.linkStr("account/register", null))));
			dataMap.put("text_note",language.get("text_note"));
			dataMap.put("text_tags",language.get("text_tags"));
			dataMap.put("text_related",language.get("text_related"));
			dataMap.put("text_payment_recurring",language.get("text_payment_recurring"));
			dataMap.put("text_loading",language.get("text_loading"));

			dataMap.put("entry_qty",language.get("entry_qty"));
			dataMap.put("entry_name",language.get("entry_name"));
			dataMap.put("entry_review",language.get("entry_review"));
			dataMap.put("entry_rating",language.get("entry_rating"));
			dataMap.put("entry_good",language.get("entry_good"));
			dataMap.put("entry_bad",language.get("entry_bad"));

			dataMap.put("button_cart",language.get("button_cart"));
			dataMap.put("button_wishlist",language.get("button_wishlist"));
			dataMap.put("button_compare",language.get("button_compare"));
			dataMap.put("button_upload",language.get("button_upload"));
			dataMap.put("button_continue",language.get("button_continue"));
			
			
			
			dataMap.put("tab_description",language.get("tab_description"));
			dataMap.put("tab_attribute",language.get("tab_attribute"));
			dataMap.put("tab_review",  MemoryUtils.valueOf(String.format(language.getStr("tab_review"), productInfo.get("reviews"))));
			dataMap.put("product_id",  MemoryUtils.valueOf(productId));
			dataMap.put("manufacturer",  MemoryUtils.valueOf(productInfo.getStr("manufacturer")));
			dataMap.put("manufacturers",  url.link("product/manufacturer/info", "manufacturer_id="+productInfo.get("manufacturer_id")));
			dataMap.put("model",  MemoryUtils.valueOf(productInfo.getStr("model")));
			dataMap.put("reward",  MemoryUtils.valueOf(productInfo.get("reward")));
			dataMap.put("points",  MemoryUtils.valueOf(productInfo.get("points")));
			dataMap.put("description",  MemoryUtils.valueOf(StringEscapeUtils.unescapeHtml4(productInfo.getStr("description"))));
			if (productInfo.getInt("quantity") <= 0) {
				dataMap.put("stock",  MemoryUtils.valueOf(productInfo.getStr("stock_status")));
			} else if (StrKit.notBlank((String)config.get("config_stock_display")) && !"0".equals(config.get("config_stock_display"))) {
				dataMap.put("stock",  MemoryUtils.valueOf(productInfo.get("quantity")));
			} else {
				dataMap.put("stock",language.get("text_instock"));
			}
			
			String image = productInfo.getStr("image");
			if (StrKit.notBlank(image)) {
				dataMap.put("popup", MemoryUtils.valueOf(ImageKit.resize(image, config.get(config.get("config_theme")+"_image_popup_width"), config.get(config.get("config_theme")+"_image_popup_height"))));
			} else {
				dataMap.put("popup",new StringMemory(""));
			}
			if (StrKit.notBlank(image)) {
				dataMap.put("thumb", MemoryUtils.valueOf(ImageKit.resize(image, config.get(config.get("config_theme")+"_image_thumb_width"), config.get(config.get("config_theme")+"_image_thumb_height"))));
			} else {
				dataMap.put("thumb",new StringMemory(""));
			}
			List<ProductImage> productImages = CatalogProductService.ME.getProductImages(productId);
			List<Map<String, Object>> images = new ArrayList<Map<String,Object>>();
			for (ProductImage productImage : productImages) {
				images.add(ChainMap.createMap().put("popup", ImageKit.resize(productImage.getStr("image"), config.get(config.get("config_theme")+"_image_popup_width"), config.get(config.get("config_theme")+"_image_popup_height")))
							.put("thumb", ImageKit.resize(productImage.getStr("image"), config.get(config.get("config_theme")+"_image_thumb_width"), config.get(config.get("config_theme")+"_image_thumb_height"))).toMap()
						);
			}
			dataMap.put("images", MemoryUtils.valueOf(images));
			String priceValue = null;
			BigDecimal price = productInfo.getBigDecimal("price");
			if(customer.isLogged() || config.isset("config_customer_price")){
				priceValue = currency.format(tax.calculate(price, productInfo.getInt("tax_class_id"), (String)config.get("config_tax")), (String)getSessionAttr("currency"), null);
			}
			String specialValue = null;
			BigDecimal special = productInfo.getBigDecimal("special");
			if(special != null){
				specialValue = currency.format(tax.calculate(special, productInfo.getInt("tax_class_id"), (String)config.get("config_tax")), (String)getSessionAttr("currency"), null);
			}
			String taxValue = null;
			if(config.isset("config_tax")){
				taxValue = currency.format(special == null ?price : special, (String)getSessionAttr("currency"), null);
			}
			
			List<Map<String, Object>> discounts = new ArrayList<Map<String,Object>>();
			for (ProductDiscount discount : CatalogProductService.ME.getProductDiscounts(productId)) {
				Map<String, Object> discountMap  =new HashMap<String, Object>();
				discountMap.put("quantity", discount.get("quantity"));
				discountMap.put("price", currency.format(
						tax.calculate(discount.getBigDecimal("price"), productInfo.getInt("tax_class_id"), (String)config.get("config_tax")), (String)getSessionAttr("currency"), null));	
				discounts.add(discountMap);
			}
			dataMap.put("discounts", MemoryUtils.valueOf(discounts));
			
			List<ProductOption> productOptions = CatalogProductService.ME.getProductOptions(productId);
			List<Map<String, Object>> options = new ArrayList<Map<String,Object>>();
			for (ProductOption productOption : productOptions) {
				List<Map<String, Object>> productOptionValueData = new ArrayList<Map<String,Object>>();
				
				List<ProductOptionValue> productOptionValue = productOption.get("product_option_value");
				for (ProductOptionValue optionValue : productOptionValue) {
					String optionPriceValue = null;
					if((config.isset("config_customer_price") && customer.isLogged())
							||!config.isset("config_customer_price")  && SetKit.isset(optionValue.get("price"))
							){
						optionPriceValue = currency.format(tax.calculate(optionValue.getBigDecimal("price"), productInfo.getInt("tax_class_id"), config.isset("config_tax")?"P":null)
							, (String)getSessionAttr("currency"), null);
					}
					productOptionValueData.add(
							ChainMap.createMap()
							.put("product_option_value_id", optionValue.get("product_option_value_id"))
							.put("option_value_id", optionValue.get("option_value_id"))
							.put("name", optionValue.get("name"))
							.put("image", ImageKit.resize(optionValue.getStr("image"), 50, 50))
							.put("price", optionPriceValue)
							.put("price_prefix", optionValue.get("price_prefix"))
							.toMap());
				}
				options.add(ChainMap.createMap()
						.put("product_option_id", productOption.get("product_option_id"))
						.put("product_option_value", productOptionValueData)
						.put("option_id", productOption.get("option_id"))
						.put("name", productOption.get("name"))
						.put("type", productOption.get("type"))
						.put("value", productOption.get("value"))
						.put("required", productOption.get("required"))
						.toMap()
						);
			}
			dataMap.put("options", MemoryUtils.valueOf(options));
			
			Integer minimum = 1;
			if (SetKit.isset(productInfo.getInt("minimum"))) {
				minimum = productInfo.getInt("minimum");
			}
			dataMap.put("minimum", MemoryUtils.valueOf(minimum));
			dataMap.put("review_status", MemoryUtils.valueOf(config.get("config_review_status")));
			dataMap.put("review_guest", MemoryUtils.valueOf( (customer.isLogged() || config.isset("config_review_guest")) ));
			String customername = "";
			if(customer.isLogged()){
				customername = customer.getFirstname() + "&nbsp;" + customer.getLastname();
			}
			dataMap.put("customer_name", MemoryUtils.valueOf( customername ));
			dataMap.put("reviews",MemoryUtils.valueOf(String.format(language.getStr("text_reviews"), productInfo.get("reviews"))));
			dataMap.put("rating",MemoryUtils.valueOf(productInfo.get("rating")));
			dataMap.put("share",url.link("product/product", "product_id="+productId));
			dataMap.put("price", MemoryUtils.valueOf(priceValue));
			dataMap.put("special", MemoryUtils.valueOf(specialValue));
			dataMap.put("tax", MemoryUtils.valueOf(taxValue));
			//商品属性
			dataMap.put("attribute_groups", MemoryUtils.valueOf(CatalogProductService.ME.getProductAttributes(productId)));
			//相关商品
			List<Map<String, Object>> products = new ArrayList<Map<String,Object>>();
			Map<Integer, Product> productRelated = CatalogProductService.ME.getProductRelated(productId);
			for (Map.Entry<Integer, Product> entry: productRelated.entrySet()) {
				Product product = entry.getValue();
				if(SetKit.isset(product.getStr("image"))){
					image = ImageKit.resize(product.getStr("image"), config.get(config.get("config_theme")+"_image_related_width") , config.get(config.get("config_theme")+"_image_related_height"));
				}else{
					image = ImageKit.resize("placeholder.png", config.get(config.get("config_theme")+"_image_related_width") , config.get(config.get("config_theme")+"_image_related_height"));
				}
				price = product.getBigDecimal("price");
				if(customer.isLogged() || config.isset("config_customer_price")){
					priceValue = currency.format(tax.calculate(price, product.getInt("tax_class_id"), (String)config.get("config_tax")), (String)getSessionAttr("currency"), null);
				}
				special = product.getBigDecimal("special");
				if(special != null){
					specialValue = currency.format(tax.calculate(special, product.getInt("tax_class_id"), (String)config.get("config_tax")), (String)getSessionAttr("currency"), null);
				}
				if(config.isset("config_tax")){
					taxValue = currency.format(special == null ?price : special, (String)getSessionAttr("currency"), null);
				}
				long rating = 0;
				if(config.isset("config_review_status")){
					rating = product.getLong("rating");
				}
				products.add(
						ChainMap.createMap()
						.put("product_id", product.get("product_id"))
						.put("thumb", image)
						.put("name", product.get("name"))
						.put("description", StrTool.subStrClearTag(StringEscapeUtils.unescapeHtml4(product.getStr("description")), 0, 100))
						.put("price", priceValue)
						.put("special", specialValue)
						.put("tax", taxValue)
						.put("minimum", product.getInt("minimum") > 0 ? product.getInt("minimum") :1)
						.put("rating", rating)
						.put("href", url.linkStr("product/product", "product_id="+product.getInt("product_id")))
						.toMap()
						);
			}
			dataMap.put("products", MemoryUtils.valueOf(products));
			//商品标签
			List<Map<String, Object>> tags = new ArrayList<Map<String,Object>>();
			if(SetKit.isset(productInfo.getStr("tag"))){
				for (String tag : productInfo.getStr("tag").split(",")) {
					tags.add(
							ChainMap.createMap()
							.put("tag", tag.trim())
							.put("href", url.linkStr("product/search", "tag="+tag.trim()))
							.toMap());
				}
			}
			dataMap.put("tags", MemoryUtils.valueOf(tags));
			//获取周期
			List<Map<String, Object>> recurrings = CatalogProductService.ME.getProfiles(productId);
			dataMap.put("recurrings", MemoryUtils.valueOf(recurrings));
			//更新浏览次数
			CatalogProductService.ME.updateViewed(productId);
			//其他组件
			dataMap.put("breadcrumbs", MemoryUtils.valueOf(breadcrumbs));	//导航
			dataMap.put("column_left", MemoryUtils.valueOf(load.module(new CommonColumnLeftModule(this))));
			dataMap.put("column_right", MemoryUtils.valueOf(load.module(new CommonColumnRightModule(this))));
			dataMap.put("content_top", MemoryUtils.valueOf(load.module(new CommonContentTopModule(this))));
			dataMap.put("content_bottom", MemoryUtils.valueOf(load.module(new CommonContentBottomModule(this))));
			dataMap.put("footer", MemoryUtils.valueOf(load.module(new CommonFooterModule(this))));
			dataMap.put("header", MemoryUtils.valueOf(load.module(new CommonHeaderModule(this))));
			render(new PhpRender("/catalog/view/theme/default/template/product/product.tpl", dataMap));
		}else{
			//商品404
			StringBuilder urlBuilder = new StringBuilder();
			if(StrKit.notBlank(getPara("path"))){
				urlBuilder.append("&path=").append(getPara("path"));
			}
			if(StrKit.notBlank(getPara("filter"))){
				urlBuilder.append("&filter=").append(getPara("filter"));
			}
			if(StrKit.notBlank(getPara("manufacturer_id"))){
				urlBuilder.append("&manufacturer_id=").append(getPara("manufacturer_id"));
			}
			if(StrKit.notBlank(getPara("search"))){
				urlBuilder.append("&search=").append(getPara("search"));
			}
			if(StrKit.notBlank(getPara("tag"))){
				urlBuilder.append("&tag=").append(getPara("tag"));
			}
			if(StrKit.notBlank(getPara("description"))){
				urlBuilder.append("&description=").append(getPara("description"));
			}
			if(StrKit.notBlank(getPara("category_id"))){
				urlBuilder.append("&category_id=").append(getPara("category_id"));
			}
			if(StrKit.notBlank(getPara("sub_category"))){
				urlBuilder.append("&sub_category=").append(getPara("sub_category"));
			}
			if(StrKit.notBlank(getPara("sort"))){
				urlBuilder.append("&sort=").append(getPara("sort"));
			}
			if(StrKit.notBlank(getPara("order"))){
				urlBuilder.append("&order=").append(getPara("order"));
			}
			if(StrKit.notBlank(getPara("page"))){
				urlBuilder.append("&page=").append(getPara("page"));
			}
			if(StrKit.notBlank(getPara("limit"))){
				urlBuilder.append("&limit=").append(getPara("limit"));
			}
			urlBuilder.append("&product_id=").append(getPara("product_id"));
			breadcrumbs.add(ChainMap.createMap()
					.put("text", language.getStr("text_error"))
					.put("href", url.linkStr("product/product",urlBuilder.toString()))
					.toMap());
			
			document.setTitle(language.getStr("text_error"));
			dataMap.put("heading_title",language.get("text_error"));
			dataMap.put("text_error",language.get("text_error"));
			dataMap.put("button_continue",language.get("button_continue"));
			dataMap.put("continue",url.link("common/home", null));
			dataMap.put("breadcrumbs", MemoryUtils.valueOf(breadcrumbs));	//导航
			dataMap.put("column_left", MemoryUtils.valueOf(load.module(new CommonColumnLeftModule(this))));
			dataMap.put("column_right", MemoryUtils.valueOf(load.module(new CommonColumnRightModule(this))));
			dataMap.put("content_top", MemoryUtils.valueOf(load.module(new CommonContentTopModule(this))));
			dataMap.put("content_bottom", MemoryUtils.valueOf(load.module(new CommonContentBottomModule(this))));
			dataMap.put("footer", MemoryUtils.valueOf(load.module(new CommonFooterModule(this))));
			dataMap.put("header", MemoryUtils.valueOf(load.module(new CommonHeaderModule(this))));
			getResponse().setStatus(404);
			render(new PhpRender("/catalog/view/theme/default/template/error/not_found.tpl", dataMap));
		}
	}
	
	/**
	 * 评价
	 */
	public void review(){
		render(new HtmlRender(""));
	}
	
}
