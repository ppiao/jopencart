package cn.javacart.jopencart.controller.catalog.module;

import php.runtime.memory.support.MemoryUtils;

import com.jfplugin.kit.mockrender.MockRenderKit;

import cn.javacart.jfinal.php.render.PhpRender;
import cn.javacart.jopencart.engine.JOpencartController;
import cn.javacart.jopencart.engine.JOpencartModule;

/**
 * 账号组件
 * @author farmer
 *
 */
public class ModuleAccountModule extends JOpencartModule{
	
	public ModuleAccountModule(JOpencartController controller) {
		super(controller);
	}
	
	@Override
	public String render() {
		language.load("/catalog/language/{0}/module/account.php");
		dataMap.put("heading_title", language.get("heading_title"));
		dataMap.put("text_register",language.get("text_register"));
		dataMap.put("text_login",language.get("text_login"));
		dataMap.put("text_logout",language.get("text_logout"));
		dataMap.put("text_forgotten",language.get("text_forgotten"));
		dataMap.put("text_account",language.get("text_account"));
		dataMap.put("text_edit",language.get("text_edit"));
		dataMap.put("text_password",language.get("text_password"));
		dataMap.put("text_address",language.get("text_address"));
		dataMap.put("text_wishlist",language.get("text_wishlist"));
		dataMap.put("text_order",language.get("text_order"));
		dataMap.put("text_download",language.get("text_download"));
		dataMap.put("text_reward",language.get("text_reward"));
		dataMap.put("text_return",language.get("text_return"));
		dataMap.put("text_transaction",language.get("text_transaction"));
		dataMap.put("text_newsletter",language.get("text_newsletter"));
		dataMap.put("text_recurring",language.get("text_recurring"));
		dataMap.put("logged",MemoryUtils.valueOf(customer.isLogged()));
		dataMap.put("register",url.link("account/register",null));
		dataMap.put("login",url.link("account/login",null));
		dataMap.put("logout",url.link("account/logout",null));
		dataMap.put("forgotten",url.link("account/forgotten",null));
		dataMap.put("account",url.link("account/account",null));
		dataMap.put("edit",url.link("account/edit",null));
		dataMap.put("password",url.link("account/password",null));
		dataMap.put("address",url.link("account/address",null));
		dataMap.put("wishlist",url.link("account/wishlist",null));
		dataMap.put("order",url.link("account/order",null));
		dataMap.put("download",url.link("account/download",null));
		dataMap.put("reward",url.link("account/reward",null));
		dataMap.put("return",url.link("account/return",null));
		dataMap.put("transaction",url.link("account/transaction",null));
		dataMap.put("newsletter",url.link("account/newsletter",null));
		dataMap.put("recurring",url.link("account/recurring",null));

		
		return MockRenderKit.render(new PhpRender("/catalog/view/theme/default/template/module/account.tpl", dataMap));
	}

}
