/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_affiliate表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|affiliate_id        |INT(10)             |false |true    |NULL    |
|firstname           |VARCHAR(32)         |false |false   |NULL    |
|lastname            |VARCHAR(32)         |false |false   |NULL    |
|email               |VARCHAR(96)         |false |false   |NULL    |
|telephone           |VARCHAR(32)         |false |false   |NULL    |
|fax                 |VARCHAR(32)         |false |false   |NULL    |
|password            |VARCHAR(40)         |false |false   |NULL    |
|salt                |VARCHAR(9)          |false |false   |NULL    |
|company             |VARCHAR(40)         |false |false   |NULL    |
|website             |VARCHAR(255)        |false |false   |NULL    |
|address_1           |VARCHAR(128)        |false |false   |NULL    |
|address_2           |VARCHAR(128)        |false |false   |NULL    |
|city                |VARCHAR(128)        |false |false   |NULL    |
|postcode            |VARCHAR(10)         |false |false   |NULL    |
|country_id          |INT(10)             |false |false   |NULL    |
|zone_id             |INT(10)             |false |false   |NULL    |
|code                |VARCHAR(64)         |false |false   |NULL    |
|commission          |DECIMAL(4)          |false |false   |0.00|
|tax                 |VARCHAR(64)         |false |false   |NULL    |
|payment             |VARCHAR(6)          |false |false   |NULL    |
|cheque              |VARCHAR(100)        |false |false   |NULL    |
|paypal              |VARCHAR(64)         |false |false   |NULL    |
|bank_name           |VARCHAR(64)         |false |false   |NULL    |
|bank_branch_number  |VARCHAR(64)         |false |false   |NULL    |
|bank_swift_code     |VARCHAR(64)         |false |false   |NULL    |
|bank_account_name   |VARCHAR(64)         |false |false   |NULL    |
|bank_account_number |VARCHAR(64)         |false |false   |NULL    |
|ip                  |VARCHAR(40)         |false |false   |NULL    |
|status              |BIT(0)              |false |false   |NULL    |
|approved            |BIT(0)              |false |false   |NULL    |
|date_added          |DATETIME(19)        |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class Affiliate extends Model<Affiliate>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2333451483282233345L;
	/**
	 * 用于查询操作
	 */
	public static final Affiliate ME = new Affiliate();
	
}