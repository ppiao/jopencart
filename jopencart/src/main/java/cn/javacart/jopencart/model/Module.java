/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_module表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|module_id           |INT(10)             |false |true    |NULL    |
|name                |VARCHAR(64)         |false |false   |NULL    |
|code                |VARCHAR(32)         |false |false   |NULL    |
|setting             |TEXT(65535)         |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class Module extends Model<Module>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2423531483282242353L;
	/**
	 * 用于查询操作
	 */
	public static final Module ME = new Module();
	
}