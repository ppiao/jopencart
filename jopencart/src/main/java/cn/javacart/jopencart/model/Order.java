/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_order表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|order_id            |INT(10)             |false |true    |NULL    |
|invoice_no          |INT(10)             |false |false   |0|
|invoice_prefix      |VARCHAR(26)         |false |false   |NULL    |
|store_id            |INT(10)             |false |false   |0|
|store_name          |VARCHAR(64)         |false |false   |NULL    |
|store_url           |VARCHAR(255)        |false |false   |NULL    |
|customer_id         |INT(10)             |false |false   |0|
|customer_group_id   |INT(10)             |false |false   |0|
|firstname           |VARCHAR(32)         |false |false   |NULL    |
|lastname            |VARCHAR(32)         |false |false   |NULL    |
|email               |VARCHAR(96)         |false |false   |NULL    |
|telephone           |VARCHAR(32)         |false |false   |NULL    |
|fax                 |VARCHAR(32)         |false |false   |NULL    |
|custom_field        |TEXT(65535)         |false |false   |NULL    |
|payment_firstname   |VARCHAR(32)         |false |false   |NULL    |
|payment_lastname    |VARCHAR(32)         |false |false   |NULL    |
|payment_company     |VARCHAR(40)         |false |false   |NULL    |
|payment_address_1   |VARCHAR(128)        |false |false   |NULL    |
|payment_address_2   |VARCHAR(128)        |false |false   |NULL    |
|payment_city        |VARCHAR(128)        |false |false   |NULL    |
|payment_postcode    |VARCHAR(10)         |false |false   |NULL    |
|payment_country     |VARCHAR(128)        |false |false   |NULL    |
|payment_country_id  |INT(10)             |false |false   |NULL    |
|payment_zone        |VARCHAR(128)        |false |false   |NULL    |
|payment_zone_id     |INT(10)             |false |false   |NULL    |
|payment_address_format|TEXT(65535)         |false |false   |NULL    |
|payment_custom_field|TEXT(65535)         |false |false   |NULL    |
|payment_method      |VARCHAR(128)        |false |false   |NULL    |
|payment_code        |VARCHAR(128)        |false |false   |NULL    |
|shipping_firstname  |VARCHAR(32)         |false |false   |NULL    |
|shipping_lastname   |VARCHAR(32)         |false |false   |NULL    |
|shipping_company    |VARCHAR(40)         |false |false   |NULL    |
|shipping_address_1  |VARCHAR(128)        |false |false   |NULL    |
|shipping_address_2  |VARCHAR(128)        |false |false   |NULL    |
|shipping_city       |VARCHAR(128)        |false |false   |NULL    |
|shipping_postcode   |VARCHAR(10)         |false |false   |NULL    |
|shipping_country    |VARCHAR(128)        |false |false   |NULL    |
|shipping_country_id |INT(10)             |false |false   |NULL    |
|shipping_zone       |VARCHAR(128)        |false |false   |NULL    |
|shipping_zone_id    |INT(10)             |false |false   |NULL    |
|shipping_address_format|TEXT(65535)         |false |false   |NULL    |
|shipping_custom_field|TEXT(65535)         |false |false   |NULL    |
|shipping_method     |VARCHAR(128)        |false |false   |NULL    |
|shipping_code       |VARCHAR(128)        |false |false   |NULL    |
|comment             |TEXT(65535)         |false |false   |NULL    |
|total               |DECIMAL(15)         |false |false   |0.0000|
|order_status_id     |INT(10)             |false |false   |0|
|affiliate_id        |INT(10)             |false |false   |NULL    |
|commission          |DECIMAL(15)         |false |false   |NULL    |
|marketing_id        |INT(10)             |false |false   |NULL    |
|tracking            |VARCHAR(64)         |false |false   |NULL    |
|language_id         |INT(10)             |false |false   |NULL    |
|currency_id         |INT(10)             |false |false   |NULL    |
|currency_code       |VARCHAR(3)          |false |false   |NULL    |
|currency_value      |DECIMAL(15)         |false |false   |1.00000000|
|ip                  |VARCHAR(40)         |false |false   |NULL    |
|forwarded_ip        |VARCHAR(40)         |false |false   |NULL    |
|user_agent          |VARCHAR(255)        |false |false   |NULL    |
|accept_language     |VARCHAR(255)        |false |false   |NULL    |
|date_added          |DATETIME(19)        |false |false   |NULL    |
|date_modified       |DATETIME(19)        |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class Order extends Model<Order>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2430511483282243051L;
	/**
	 * 用于查询操作
	 */
	public static final Order ME = new Order();
	
}