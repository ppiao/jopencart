/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_customer表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|customer_id         |INT(10)             |false |true    |NULL    |
|customer_group_id   |INT(10)             |false |false   |NULL    |
|store_id            |INT(10)             |false |false   |0|
|firstname           |VARCHAR(32)         |false |false   |NULL    |
|lastname            |VARCHAR(32)         |false |false   |NULL    |
|email               |VARCHAR(96)         |false |false   |NULL    |
|telephone           |VARCHAR(32)         |false |false   |NULL    |
|fax                 |VARCHAR(32)         |false |false   |NULL    |
|password            |VARCHAR(40)         |false |false   |NULL    |
|salt                |VARCHAR(9)          |false |false   |NULL    |
|cart                |TEXT(65535)         |true  |false   |NULL    |
|wishlist            |TEXT(65535)         |true  |false   |NULL    |
|newsletter          |BIT(0)              |false |false   |0|
|address_id          |INT(10)             |false |false   |0|
|custom_field        |TEXT(65535)         |false |false   |NULL    |
|ip                  |VARCHAR(40)         |false |false   |NULL    |
|status              |BIT(0)              |false |false   |NULL    |
|approved            |BIT(0)              |false |false   |NULL    |
|safe                |BIT(0)              |false |false   |NULL    |
|token               |TEXT(65535)         |false |false   |NULL    |
|code                |VARCHAR(40)         |false |false   |NULL    |
|date_added          |DATETIME(19)        |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class Customer extends Model<Customer>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2378541483282237854L;
	/**
	 * 用于查询操作
	 */
	public static final Customer ME = new Customer();
	
}