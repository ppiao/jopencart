/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_url_alias表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|url_alias_id        |INT(10)             |false |true    |NULL    |
|query               |VARCHAR(255)        |false |false   |NULL    |
|keyword             |VARCHAR(255)        |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class UrlAlias extends Model<UrlAlias>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2485341483282248534L;
	/**
	 * 用于查询操作
	 */
	public static final UrlAlias ME = new UrlAlias();
	
}