/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_order_custom_field表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|order_custom_field_id|INT(10)             |false |true    |NULL    |
|order_id            |INT(10)             |false |false   |NULL    |
|custom_field_id     |INT(10)             |false |false   |NULL    |
|custom_field_value_id|INT(10)             |false |false   |NULL    |
|name                |VARCHAR(255)        |false |false   |NULL    |
|value               |TEXT(65535)         |false |false   |NULL    |
|type                |VARCHAR(32)         |false |false   |NULL    |
|location            |VARCHAR(16)         |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class OrderCustomField extends Model<OrderCustomField>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2431861483282243186L;
	/**
	 * 用于查询操作
	 */
	public static final OrderCustomField ME = new OrderCustomField();
	
}