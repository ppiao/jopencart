/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_customer_wishlist表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|customer_id         |INT(10)             |false |true    |NULL    |
|product_id          |INT(10)             |false |true    |NULL    |
|date_added          |DATETIME(19)        |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class CustomerWishlist extends Model<CustomerWishlist>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2391231483282239123L;
	/**
	 * 用于查询操作
	 */
	public static final CustomerWishlist ME = new CustomerWishlist();
	
}