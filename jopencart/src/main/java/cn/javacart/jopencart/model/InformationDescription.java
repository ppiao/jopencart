/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_information_description表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|information_id      |INT(10)             |false |true    |NULL    |
|language_id         |INT(10)             |false |true    |NULL    |
|title               |VARCHAR(64)         |false |false   |NULL    |
|description         |TEXT(65535)         |false |false   |NULL    |
|meta_title          |VARCHAR(255)        |false |false   |NULL    |
|meta_description    |VARCHAR(255)        |false |false   |NULL    |
|meta_keyword        |VARCHAR(255)        |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class InformationDescription extends Model<InformationDescription>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2405421483282240542L;
	/**
	 * 用于查询操作
	 */
	public static final InformationDescription ME = new InformationDescription();
	
}