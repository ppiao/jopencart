/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_weight_class_description表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|weight_class_id     |INT(10)             |false |true    |NULL    |
|language_id         |INT(10)             |false |true    |NULL    |
|title               |VARCHAR(32)         |false |false   |NULL    |
|unit                |VARCHAR(4)          |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class WeightClassDescription extends Model<WeightClassDescription>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2495801483282249580L;
	/**
	 * 用于查询操作
	 */
	public static final WeightClassDescription ME = new WeightClassDescription();
	
}