/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_cart表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|cart_id             |INT UNSIGNED(10)    |false |true    |NULL    |
|customer_id         |INT(10)             |false |false   |NULL    |
|session_id          |VARCHAR(32)         |false |false   |NULL    |
|product_id          |INT(10)             |false |false   |NULL    |
|recurring_id        |INT(10)             |false |false   |NULL    |
|option              |TEXT(65535)         |false |false   |NULL    |
|quantity            |INT(10)             |false |false   |NULL    |
|date_added          |DATETIME(19)        |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class Cart extends Model<Cart>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2353211483282235321L;
	/**
	 * 用于查询操作
	 */
	public static final Cart ME = new Cart();
	
}