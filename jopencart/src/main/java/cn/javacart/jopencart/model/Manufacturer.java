/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_manufacturer表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|manufacturer_id     |INT(10)             |false |true    |NULL    |
|name                |VARCHAR(64)         |false |false   |NULL    |
|image               |VARCHAR(255)        |true  |false   |NULL    |
|sort_order          |INT(10)             |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class Manufacturer extends Model<Manufacturer>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2418271483282241827L;
	/**
	 * 用于查询操作
	 */
	public static final Manufacturer ME = new Manufacturer();
	
}