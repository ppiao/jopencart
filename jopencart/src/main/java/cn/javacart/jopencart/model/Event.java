/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_event表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|event_id            |INT(10)             |false |true    |NULL    |
|code                |VARCHAR(32)         |false |false   |NULL    |
|trigger             |TEXT(65535)         |false |false   |NULL    |
|action              |TEXT(65535)         |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class Event extends Model<Event>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2395231483282239523L;
	/**
	 * 用于查询操作
	 */
	public static final Event ME = new Event();
	
}