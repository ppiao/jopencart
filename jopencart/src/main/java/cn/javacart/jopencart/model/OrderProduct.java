/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_order_product表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|order_product_id    |INT(10)             |false |true    |NULL    |
|order_id            |INT(10)             |false |false   |NULL    |
|product_id          |INT(10)             |false |false   |NULL    |
|name                |VARCHAR(255)        |false |false   |NULL    |
|model               |VARCHAR(64)         |false |false   |NULL    |
|quantity            |INT(10)             |false |false   |NULL    |
|price               |DECIMAL(15)         |false |false   |0.0000|
|total               |DECIMAL(15)         |false |false   |0.0000|
|tax                 |DECIMAL(15)         |false |false   |0.0000|
|reward              |INT(10)             |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class OrderProduct extends Model<OrderProduct>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2435921483282243592L;
	/**
	 * 用于查询操作
	 */
	public static final OrderProduct ME = new OrderProduct();
	
}