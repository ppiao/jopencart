/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_order_recurring表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|order_recurring_id  |INT(10)             |false |true    |NULL    |
|order_id            |INT(10)             |false |false   |NULL    |
|reference           |VARCHAR(255)        |false |false   |NULL    |
|product_id          |INT(10)             |false |false   |NULL    |
|product_name        |VARCHAR(255)        |false |false   |NULL    |
|product_quantity    |INT(10)             |false |false   |NULL    |
|recurring_id        |INT(10)             |false |false   |NULL    |
|recurring_name      |VARCHAR(255)        |false |false   |NULL    |
|recurring_description|VARCHAR(255)        |false |false   |NULL    |
|recurring_frequency |VARCHAR(25)         |false |false   |NULL    |
|recurring_cycle     |SMALLINT(5)         |false |false   |NULL    |
|recurring_duration  |SMALLINT(5)         |false |false   |NULL    |
|recurring_price     |DECIMAL(10)         |false |false   |NULL    |
|trial               |BIT(0)              |false |false   |NULL    |
|trial_frequency     |VARCHAR(25)         |false |false   |NULL    |
|trial_cycle         |SMALLINT(5)         |false |false   |NULL    |
|trial_duration      |SMALLINT(5)         |false |false   |NULL    |
|trial_price         |DECIMAL(10)         |false |false   |NULL    |
|status              |TINYINT(3)          |false |false   |NULL    |
|date_added          |DATETIME(19)        |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class OrderRecurring extends Model<OrderRecurring>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2437201483282243720L;
	/**
	 * 用于查询操作
	 */
	public static final OrderRecurring ME = new OrderRecurring();
	
}