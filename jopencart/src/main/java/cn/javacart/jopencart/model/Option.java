/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_option表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|option_id           |INT(10)             |false |true    |NULL    |
|type                |VARCHAR(32)         |false |false   |NULL    |
|sort_order          |INT(10)             |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class Option extends Model<Option>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2424831483282242483L;
	/**
	 * 用于查询操作
	 */
	public static final Option ME = new Option();
	
}