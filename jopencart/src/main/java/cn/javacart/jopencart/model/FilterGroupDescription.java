/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_filter_group_description表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|filter_group_id     |INT(10)             |false |true    |NULL    |
|language_id         |INT(10)             |false |true    |NULL    |
|name                |VARCHAR(64)         |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class FilterGroupDescription extends Model<FilterGroupDescription>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2401561483282240156L;
	/**
	 * 用于查询操作
	 */
	public static final FilterGroupDescription ME = new FilterGroupDescription();
	
}