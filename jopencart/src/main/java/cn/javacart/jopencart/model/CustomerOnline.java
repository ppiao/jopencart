/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_customer_online表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|ip                  |VARCHAR(40)         |false |true    |NULL    |
|customer_id         |INT(10)             |false |false   |NULL    |
|url                 |TEXT(65535)         |false |false   |NULL    |
|referer             |TEXT(65535)         |false |false   |NULL    |
|date_added          |DATETIME(19)        |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class CustomerOnline extends Model<CustomerOnline>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2387451483282238745L;
	/**
	 * 用于查询操作
	 */
	public static final CustomerOnline ME = new CustomerOnline();
	
}