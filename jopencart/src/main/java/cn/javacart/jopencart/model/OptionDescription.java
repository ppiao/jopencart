/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_option_description表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|option_id           |INT(10)             |false |true    |NULL    |
|language_id         |INT(10)             |false |true    |NULL    |
|name                |VARCHAR(128)        |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class OptionDescription extends Model<OptionDescription>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2426311483282242631L;
	/**
	 * 用于查询操作
	 */
	public static final OptionDescription ME = new OptionDescription();
	
}