/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_download表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|download_id         |INT(10)             |false |true    |NULL    |
|filename            |VARCHAR(160)        |false |false   |NULL    |
|mask                |VARCHAR(128)        |false |false   |NULL    |
|date_added          |DATETIME(19)        |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class Download extends Model<Download>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2392521483282239252L;
	/**
	 * 用于查询操作
	 */
	public static final Download ME = new Download();
	
}