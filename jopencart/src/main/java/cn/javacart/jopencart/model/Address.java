/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_address表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|address_id          |INT(10)             |false |true    |NULL    |
|customer_id         |INT(10)             |false |false   |NULL    |
|firstname           |VARCHAR(32)         |false |false   |NULL    |
|lastname            |VARCHAR(32)         |false |false   |NULL    |
|company             |VARCHAR(40)         |false |false   |NULL    |
|address_1           |VARCHAR(128)        |false |false   |NULL    |
|address_2           |VARCHAR(128)        |false |false   |NULL    |
|city                |VARCHAR(128)        |false |false   |NULL    |
|postcode            |VARCHAR(10)         |false |false   |NULL    |
|country_id          |INT(10)             |false |false   |0|
|zone_id             |INT(10)             |false |false   |0|
|custom_field        |TEXT(65535)         |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class Address extends Model<Address>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2331651483282233165L;
	/**
	 * 用于查询操作
	 */
	public static final Address ME = new Address();
	
}