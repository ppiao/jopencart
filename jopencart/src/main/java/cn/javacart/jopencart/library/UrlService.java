package cn.javacart.jopencart.library;

import php.runtime.Memory;
import php.runtime.memory.support.MemoryUtils;

import com.jfinal.kit.StrKit;

/**
 * URL处理
 * @author farmer
 *
 */
public class UrlService {

	private static UrlService urlService = null;
	
	private UrlService(){}
	
	/**
	 * 单例
	 * @return
	 */
	public static synchronized UrlService getInstance() {
		if (urlService == null) {
			urlService = new UrlService();
		}
		return urlService;
	}
	
	/**
	 * 返回PHP类型
	 * @param route
	 * @param args
	 * @return
	 */
	public Memory link(String route , String args){
		return MemoryUtils.valueOf(linkStr(route, args));
	}
	
	/**
	 * 返回Str
	 * @param route
	 * @param args
	 * @return
	 */
	public String linkStr(String route , String args){
		String contextPath = (String)ConfigService.getInstance().get("context_path");
		if(StrKit.notBlank(args)){
			return contextPath+"/index.php?route="+route+"&"+args;
		}
		return contextPath+"/index.php?route="+route;
	}
}
