package cn.javacart.jopencart.service.catalog.design;

import java.util.List;

import cn.javacart.jopencart.library.SessionConfigService;
import cn.javacart.jopencart.model.BannerImage;

import com.jfinal.aop.Duang;

/**
 * 
 * @author farmer
 *
 */
public class DesignBannerService {

	public final static DesignBannerService ME = Duang.duang(DesignBannerService.class);
	
	/**
	 * 获取Banner
	 * @param bannerId
	 * @return
	 */
	public List<BannerImage> getBanner(Integer bannerId){
		return BannerImage.ME.find("SELECT\n" + 
				"	*\n" + 
				"FROM\n" + 
				"	joc_banner_image bi\n" + 
				"LEFT JOIN joc_banner_image_description bid ON\n" + 
				"	(\n" + 
				"		bi.banner_image_id = bid.banner_image_id\n" + 
				"	)\n" + 
				"WHERE\n" + 
				"	bi.banner_id =?\n" + 
				"	AND bid.language_id =?\n" + 
				"ORDER BY\n" + 
				"	bi.sort_order ASC",bannerId,SessionConfigService.get("config_language_id"));
	}
	
}
