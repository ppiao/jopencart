package cn.javacart.jopencart.service.catalog.account;

import cn.javacart.jopencart.model.Address;
import cn.javacart.jopencart.model.Country;
import cn.javacart.jopencart.model.Zone;

import com.jfinal.aop.Duang;

/**
 * 用户地址服务
 * @author farmer
 *
 */
public class AccountAddressService {
	
	/**
	 * 
	 */
	public final static AccountAddressService ME = Duang.duang(AccountAddressService.class);
	
	/**
	 * 通过地址ID获取地址信息
	 * @param addressId
	 * @return
	 */
	public Address getAddress(Integer addressId){
		Address address = Address.ME.findById(addressId);
		if(address != null){
			Country country = Country.ME.findById(address.get("country_id"));
			if(country == null){
				country = new Country();
			}
			Zone zone = Zone.ME.findById(address.get("zone_id"));
			if(zone == null){
				zone = new Zone();
			}
			address.put("zone", zone.get("name"));
			address.put("zone_code", zone.get("code"));
			address.put("country", country.get("name"));
			address.put("iso_code_2", country.get("iso_code_2"));
			address.put("iso_code_3", country.get("iso_code_3"));
			address.put("address_format", country.get("address_format"));
		}
		return address;
	}
	
	/*
public function getAddress($address_id) {
		$address_query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "address WHERE address_id = '" . (int)$address_id . "' AND customer_id = '" . (int)$this->customer->getId() . "'");

		if ($address_query->num_rows) {
			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$address_query->row['country_id'] . "'");

			if ($country_query->num_rows) {
				$country = $country_query->row['name'];
				$iso_code_2 = $country_query->row['iso_code_2'];
				$iso_code_3 = $country_query->row['iso_code_3'];
				$address_format = $country_query->row['address_format'];
			} else {
				$country = '';
				$iso_code_2 = '';
				$iso_code_3 = '';
				$address_format = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$address_query->row['zone_id'] . "'");

			if ($zone_query->num_rows) {
				$zone = $zone_query->row['name'];
				$zone_code = $zone_query->row['code'];
			} else {
				$zone = '';
				$zone_code = '';
			}

			$address_data = array(
				'address_id'     => $address_query->row['address_id'],
				'firstname'      => $address_query->row['firstname'],
				'lastname'       => $address_query->row['lastname'],
				'company'        => $address_query->row['company'],
				'address_1'      => $address_query->row['address_1'],
				'address_2'      => $address_query->row['address_2'],
				'postcode'       => $address_query->row['postcode'],
				'city'           => $address_query->row['city'],
				'zone_id'        => $address_query->row['zone_id'],
				'zone'           => $zone,
				'zone_code'      => $zone_code,
				'country_id'     => $address_query->row['country_id'],
				'country'        => $country,
				'iso_code_2'     => $iso_code_2,
				'iso_code_3'     => $iso_code_3,
				'address_format' => $address_format,
				'custom_field'   => json_decode($address_query->row['custom_field'], true)
			);

			return $address_data;
		} else {
			return false;
		}
	} 
	 * */
	
	
}
