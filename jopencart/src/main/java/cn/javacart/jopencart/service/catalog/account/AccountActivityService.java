package cn.javacart.jopencart.service.catalog.account;

import java.util.Date;
import java.util.Map;

import cn.javacart.jopencart.library.RequestService;
import cn.javacart.jopencart.model.CustomerActivity;

import com.alibaba.fastjson.JSON;
import com.jfinal.aop.Duang;

/**
 * 账号活动
 * @author farmer
 *
 */
public class AccountActivityService {

	public final static AccountActivityService ME = Duang.duang(AccountActivityService.class);
	
	/**
	 * 添加记录
	 * @param key
	 * @param data
	 */
	public void addActivity(String key,Map<String,Object> data){
		if(data.get("customer_id") == null){
			data.put("customer_id", 0);
		}
		new CustomerActivity().set("customer_id", data.get("customer_id")).set("key", key)
			.set("data", JSON.toJSONString(data)).set("ip", RequestService.get().getRemoteHost())
			.set("date_added", new Date()).save();
	}
	
}
